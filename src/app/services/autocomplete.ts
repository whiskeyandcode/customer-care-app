import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { ApiService } from './api';

@Injectable()
export class AutocompleteService{
    constructor(private apiService: ApiService) { }

    searchCompanies(term: string){
        let params = new URLSearchParams();
        params.set('q', term);
        params.set('size', '5');
        return this.search(term, 'companies', params)
    }

    private extractData(data:any) {
        return data.hits.hits || [];
    }

    private search(term: string, schema?: string, params?: URLSearchParams){
        let path = '/_search';
        if (schema) {
            path = `/${schema}${path}`;
        }
        return this.apiService.get(path, params)
            .map(this.extractData);
    }
}

