import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { ApiService } from './api';
import { GenericService } from './generic';

@Injectable()
export class CompanyService extends GenericService{
    constructor(private apiService: ApiService) { super(apiService); }

    getCompany(id: string){
        let path = `/companies/${id}`;
        return this.apiService.get(path);
    }

    getCompanyPeople(company_name_id:string, q?:string){
        let path = "/people/_search";
        let params = new URLSearchParams();
        params.set('ff', 'company_id:' + company_name_id);
        params.set('size', '5');
        return this.apiService.get(path, params)
    }

    getCompanyLogData(company_name_id: string){
        let path = "/logdata/_search";
        let params = new URLSearchParams();
        params.set('ff', 'company_name_id:' + company_name_id);
        params.set('size', '100');
        params.set('o', 'tpep_dropoff_datetime');
        return this.apiService.get(path, params);
    }
}

