import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { ApiService } from './api';

@Injectable()
export class DocumentService{
    constructor(private apiService: ApiService) { }
    
    getDocuments(q?:string){
        let path = `/documents/_search`;
        let params = new URLSearchParams();
        params.set('size', '5');
        if(q){
            params.set('q', q);
        }
        return this.apiService.get(path, params);
    }

    getDocTermFrequency(q?:string){
        let path = "/documents/_search";
        let params = new URLSearchParams();
        if(q){
            params.set('q', q);
        }
        params.set('size', '0');
        params.set('aggs', 'description')
        return this.apiService.get(path, params)
    }
}