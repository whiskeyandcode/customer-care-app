import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { ApiService } from './api';

@Injectable()
export class GenericService{
    constructor(private api_service: ApiService) { }

    getOpenTickets(company_name_id?:string, q?:string){
        let path = `/tickets/_search`;
        let params = new URLSearchParams();
        if(company_name_id){
            params.set('ff', 'company_name_id:' + company_name_id);
        }
        params.set('ffor', "status:accepted");
        params.set('ffor', 'status:new');
        params.set('o', 'priority');
        params.set('size', '10');
        if(q){
            params.set('q', q);
        }
        return this.api_service.get(path, params);
    }

    getTicketStats(company_name_id?:string, q?:string){
        let path = `/tickets/_search`;
        let params = new URLSearchParams();
        if(company_name_id){
            params.set('ff', 'company_name_id:' + company_name_id);
        }
        if(q){
            params.set('q', q);
        }
        params.set('aggs', 'status,priority')
        params.set('size', '0');
        return this.api_service.get(path, params);
    }

    getDiscussions(company_name_id?:string, q?:string){
        let path = "/discussions/_search";
        let params = new URLSearchParams();
        if(company_name_id){
            params.set('ff', 'company_name_id:' + company_name_id);
        }
        params.set('size', '5');
        if(q){
            params.set('q', q);
        }
        return this.api_service.get(path, params);
    }

    getUserContribution(company_name_id?:string, q?:string){
        let path = "/discussions,documents/_search";
        let params = new URLSearchParams();
        if(company_name_id){
            params.set('ff', 'company_name_id:' + company_name_id);
        }
        if(q){
            params.set('q', q);
        }
        params.set('size', '0');
        params.set('tf.size', '3');
        params.set('aggs', 'author')
        return this.api_service.get(path, params)
    }

    getContentDistribution(company_name_id?: string, q?:string){
        let path = "/_search"
        let params = new URLSearchParams();
        params.set('aggs', '_type,title');
        params.set('size', '0');
        if(company_name_id){
            params.set('ff', 'company_name_id:' + company_name_id);
        }
        if(q){
            params.set('q', q);
        }
        return this.api_service.get(path, params);
    }
}