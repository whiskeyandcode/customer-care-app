import { provideRouter, RouterConfig }  from '@angular/router';
import { Dashboard, Company } from './containers';

const routes: RouterConfig = [
    {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full'
    },
    { path: 'dashboard', component: Dashboard },
    { path: 'company/:name/:id', component: Company },
    { path: '**', redirectTo: '/dashboard' }
];

export const APP_ROUTER_PROVIDERS = [
    provideRouter(routes)
];