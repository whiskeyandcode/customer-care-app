import { Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AutoComplete } from './autocomplete';
import { CompanyStats } from './company-stats';
import { AutocompleteService } from '../services'
import { CHART_DIRECTIVES, Highcharts } from 'angular2-highcharts';


@Component({
    selector: 'dashboard',
    template: require('!raw!../templates/dashboard.html'),
    styles: [`
        .company-stats {
            position: absolute; bottom: 50px;
            border-top: 1px solid #ccc;
            overflow-y: hidden; height: 300px;
            width: 100%;
        }
    `],
    providers: [AutocompleteService],
    directives: [
        AutoComplete,
        CompanyStats,
        CHART_DIRECTIVES
    ],
})
export class Dashboard implements OnDestroy {
    constructor(private router: Router){
        this.options = {
            chart: { type: 'area', margin: 0, height: 50,
                     animation: Highcharts.svg }, 
            legend: {enabled: false},
            yAxis: { title: { text: '' } }, xAxis: { type: 'datetime'},
            plotOptions: {
                area: {
                    fillColor: {linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                        stops: [[0, Highcharts.getOptions().colors[0]],
                                [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]]
                    },
                    marker: { radius: 2 },
                    lineWidth: 1,
                    states: { hover: { lineWidth: 1 } },
                    threshold: null
                }
            },
            series: [{
                data: (function () {
                    // generate an array of random data
                    var data:any[] = [], time = (new Date()).getTime(), i:number;
                    for (i = -100; i <= 0; i += 1) {
                        data.push({x: time + i * 1000, y: Math.random()});
                    }
                    return data;
                }())
            }]
        };
    }
    chart : HighchartsChartObject;
    options: HighchartsOptions;
    showCompanyStats: boolean = false;
    hoveredCompany: any = null;
    
    saveInstance(chartInstance:HighchartsChartObject) {
        this.chart = chartInstance;
    }

    toggleCompanySnippet(company: any){
        // toggle only if we're clicking on the same company result
        if(this.hoveredCompany !== null && this.hoveredCompany._id === company._id){
            this.showCompanyStats = !this.showCompanyStats;
        } else {
            this.hoveredCompany = company;
            this.showCompanyStats = true;
        }
    }

    hideCompanySnippet(company?: any){
        this.hoveredCompany = null;
        this.showCompanyStats = false;
    }

    ngOnDestroy(){
        this.hideCompanySnippet();
    }
}



