import { Component, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { AutocompleteService } from '../services';

@Component({
    selector: 'autocomplete-bar',
    styles: [
        require('style!../../static/css/autocomplete.css').toString()
    ],
    template: `
        <div class="autocomplete-container">
            <input class="form-control" placeholder="Search... For example Yelp" type="text" #term (keyup)="search(term.value)" />
            <ul class="autocomplete-results list-unstyled" *ngIf="resultsOpen">
                <li *ngFor="let result of results | async">
                    <div class="row">
                        <div (click)="onClick(result)" class="col-md-11 col-xs-11 result-item">
                            <div><a href="javascript:void(0)"> {{result._source.company_name}} </a></div>
                            <div>{{result._source.city}}, {{result._source.state}}</div>
                        </div>
                        <div class="col md-1 col-xs-1 text-center result-stats-icon">
                            <span class="label label-primary" (click)="onShowResultStats(result)">
                                <span class="glyphicon glyphicon-stats"></span>
                            </span>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    `
})
export class AutoComplete {
    constructor(private autocomplete: AutocompleteService,
                private router: Router){ }

    private term = new Subject<string>();


    @Output() clicked = new EventEmitter();
    @Output() onmouseleave = new EventEmitter();

    resultsOpen: boolean = false;
    results: Observable<Array<any>> = this.term
            .debounceTime(300)
            .distinctUntilChanged()
            .switchMap(term => this.autocomplete.searchCompanies(term));

    search(term: string) {
        this.resultsOpen = false;
        if(term.length > 1) {
            this.resultsOpen = true;
            this.term.next(term);
        }
    }

    onClick(item: any) {
        this.resultsOpen = false;
        this.router.navigate(['/company', item._source.company_name_id, item._id]);
    }

    onShowResultStats(item: any){
        this.clicked.next(item);
    }
}
