import {Component, Input, OnInit, OnChanges} from '@angular/core';
import { CHART_DIRECTIVES, Highcharts } from 'angular2-highcharts';

import { GenericService, StoreHelper } from '../services';
import { Store, TicketStatsOptions } from '../store';


@Component({
    selector: 'company-stats',
    directives: [CHART_DIRECTIVES],
    template: `<div class="container">
        <div class="row"><div class="col-md-12"><h2>{{company._source.title}}</h2></div></div>
        <div class="row">
            <div class="col-md-6">
                <h4>Overview</h4>
                <dl>
                  <dt>Category</dt>
                  <dd>{{company._source.company_category}}</dd>

                  <dt>Full Time Employees</dt>
                  <dd>{{company._source.full_time_employees}}</dd>
                </dl>
            </div>
            <div class="col-md-6">
                <h4>Ticket Stats</h4>
                <chart [options]="ticketStatsOptions" (load)="saveInstance($event.context)"></chart>
            </div>
        </div>
    </div>
    `
})
export class CompanyStats implements OnChanges{
    @Input() company: any;
    constructor(private genericService: GenericService,
                private store: Store,
                private storeHelper: StoreHelper){
        this.ticketStatsOptions = TicketStatsOptions;
    }
/* 
    cats:                        series
        [accepted]                    low:[1], normal:[1]
        [accepted, normal]            low:[1, 1], normal[1, 0], high[0, 1]    

*/
    private categories:any;    // new, accepted, test, fixed, invalid
    private series:any;
    ticketStatsChart: any;
    ticketStatsOptions: any;

    private redrawChart(){
        this.genericService.getTicketStats(this.company._source.company_name_id)
            .map((res:any) => res.aggregations.status_terms_filter.status_terms.buckets)
            .map((buckets: any) => {
                buckets.map((bucket:any, index:number) => {
                    this.categories.push(bucket.key.toUpperCase());
                    var priority_terms = {};
                    var unprocessed_priority_terms = Object.getOwnPropertyNames(this.series);
                    bucket.priority_terms.buckets.map((item:any) => {
                        priority_terms[item.key] = item.doc_count;
                    })
                    for(var term in priority_terms){ // for...in for dicts
                        if(this.series[term] === undefined){
                            this.series[term] = Array(index);
                            if(this.series[term].length > 0){
                                this.series[term] = this.series[term].fill(0)
                            }
                        } 
                        this.series[term].push(priority_terms[term])
                        if(unprocessed_priority_terms.indexOf(term) > -1){
                            unprocessed_priority_terms.splice(unprocessed_priority_terms.indexOf(term), 1)
                        }
                        
                    }
                    for(var term of unprocessed_priority_terms){ // for...of for arrays
                        this.series[term].push(0);
                    }
                });
                this.ticketStatsChart.xAxis[0].categories = this.categories;
                for(var item in this.series){
                    this.ticketStatsChart.addSeries({
                        name: item.toUpperCase(),
                        data: this.series[item]
                    })
                };
            })
            .subscribe();
    }
    ngOnChanges(){
        this.categories = [];
        this.series = {};
        if(this.ticketStatsChart && this.ticketStatsChart.series.length > 0){
            for(var i = this.ticketStatsChart.series.length -1; i > -1; i--) {
                this.ticketStatsChart.series[i].remove();
            }
        }
        this.redrawChart();
    }

    saveInstance(chartInstance:any){
        this.ticketStatsChart = chartInstance;
    }
}
