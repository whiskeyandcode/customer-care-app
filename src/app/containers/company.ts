import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CompanyService, DocumentService, StoreHelper } from '../services';
import { Store, LogChartOptions,
         ContributionChartOptions, TreeMapOptions } from '../store';
import { DiscussionsTable, DocumentsTable,
         ContactsTable, TicketsTable,
         Loading } from '../ui';
import { Observable }     from 'rxjs/Observable';
import { CHART_DIRECTIVES, Highcharts } from 'angular2-highcharts';
import * as moment_ from 'moment';
const moment = (<any>moment_)['default'] || moment_;

@Component({
    selector: 'company',
    template: require('!raw!../templates/company.html'),
    directives: [
        DiscussionsTable, DocumentsTable,
        ContactsTable, TicketsTable, Loading,
        CHART_DIRECTIVES
    ]
})
export class Company implements OnInit, AfterViewInit, OnDestroy{
    private sub: any;
    private company_id:string;
    private company_name_id:string;

    public company: any;
    public documents: any[];
    public tickets: any[];
    public contacts: any[];
    public discussions: any[];
    q: any;

    logChart: any;
    logChartOptions: any;

    contributionChart: any;
    contributionChartOptions: any;

    treeMap: any;
    treeMapOptions: any;
    private treeMapData:any = [];

    constructor(private route: ActivatedRoute,
                private companyService: CompanyService,
                private documentService: DocumentService,
                private store: Store,
                private storeHelper: StoreHelper) {
        this.logChartOptions = LogChartOptions;
        this.contributionChartOptions = ContributionChartOptions;
        this.treeMapOptions = TreeMapOptions;
    }

    ngOnInit(){
        this.sub = this.route.params
            .map(params => {
                this.company_id = params['id'];
                this.company_name_id = params['name'];
            })
            .flatMap(() => Observable.forkJoin(
                this.companyService
                    .getCompany(this.company_id),
                this.companyService
                    .getOpenTickets(this.company_name_id),
                this.companyService
                    .getCompanyLogData(this.company_name_id)
                    .map(res => res.hits.hits || [])
            )).subscribe(res => {
                this.company = res[0]._source;
                this.tickets = res[1].hits.hits || [];
                if (res[1].hits.total > 0){
                    this.q =  this.tickets[0]._source.title;
                    this.storeHelper.update('q', this.tickets[0]._source.title);
                }
                res[2].map((val: any) => {
                    this.logChart.series[0].addPoint([
                        val._source.tpep_dropoff_datetime,
                        val._source.total_amount
                    ]);
                });
                this.logChart.hideLoading();
            });

        this.store.changes
            .map((res: any) =>  res ? res.q : '')
            .flatMap(q => Observable.forkJoin(
                this.documentService.getDocuments(q),
                this.documentService.getDocTermFrequency(q),
                this.companyService.getDiscussions(null, q),
                this.companyService.getUserContribution(null, q)
                    .map(res => res.aggregations.author_terms_filter.author_terms.buckets),
                this.companyService
                    .getContentDistribution(null, q)
                    .map(res => res.aggregations._type_terms_filter._type_terms.buckets || [])

            ))
            .subscribe(res => {
                this.documents = res[0].hits.hits || [];
                this.discussions = res[2].hits.hits || [];

                // this.docTerm = res[1].hits.hits || [];
                this.contributionChart.series[0].setData([])
                res[3].map((item:any) => {
                    this.contributionChart.series[0].addPoint([item.key, item.doc_count])
                });
                this.contributionChart.hideLoading();

                // purple, red, orange, green
                let colors = ['#8085e9', '#f45b5b', '#f7a35c', '#90ed7d'];
                res[4].map((bucket:any) => {
                    if(['discussions', 'tickets', 'documents'].includes(bucket.key)){
                        if(bucket.title_significant_terms.buckets.length>0){
                            this.treeMapData.push({
                                id: bucket.key,
                                name: bucket.key.toUpperCase(),
                                color: colors.pop()
                            })
                            bucket.title_significant_terms.buckets
                                .map((item: any) => this.treeMapData.push({name:item.key, value:item.doc_count, parent: bucket.key}))
                        }
                    }
                });
                this.treeMap.series[0].setData(this.treeMapData);
                this.treeMapData = [];
            })
    }

    ngAfterViewInit(){
        this.logChart.showLoading();
        this.contributionChart.showLoading();
    }

    saveInstance(name:string, chartInstance:any){
        this[name] = chartInstance;
    }

    searchWithQ(){
        this.contributionChart.showLoading();
        this.storeHelper.update('q', this.q || '');
    }

    ngOnDestroy(){
        this.store.purge();
        this.sub.unsubscribe();
    }
}