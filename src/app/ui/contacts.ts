import { Component, Input } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';

@Component({
    selector: 'contacts-table',
    directives: [
    ...ROUTER_DIRECTIVES
    ],
    template: `
        <dl class="list-unstyled" *ngIf="contacts">
            <template ngFor let-contact [ngForOf]="contacts" let-i="index">
                <dt><span class="glyphicon glyphicon-user"></span>
                    {{contact._source.first_name}} {{contact._source.last_name}}
                </dt>
                <dd class="visible-*-inline-block">{{contact._source.email}}</dd>
                <dd>&nbsp;</dd>
            </template>
        </dl>
    `
})
export class ContactsTable {
    @Input() contacts:any = [];
}
