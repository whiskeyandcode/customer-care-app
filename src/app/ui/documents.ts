import { Component, Input } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';

@Component({
    selector: 'documents-table',
    directives: [
    ...ROUTER_DIRECTIVES
    ],
    template: `
        <table class="table table-striped table-condensed text-left" *ngIf="documents">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Author</th>
                </tr>
            </thead>
            <tbody>
                <tr *ngFor="let doc of documents">
                    <td>{{doc._source.title}}</td>
                    <td>{{doc._source.author}}</td>
                </tr>
            </tbody>
        </table>
    `
})
export class DocumentsTable {
    @Input() documents:any = [];
}
