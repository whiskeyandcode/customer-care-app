import { Component, Input } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';

@Component({
    selector: 'tickets-table',
    directives: [
        ...ROUTER_DIRECTIVES
    ],
    template: `
        <table class="table table-striped table-condensed text-left" *ngIf="tickets">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Type</th>
                    <th>Priority</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <tr *ngFor="let ticket of tickets">
                    <td>{{ticket._source.title}}</td>
                    <td>{{ticket._source.type}}</td>
                    <td>{{ticket._source.priority}}</td>
                    <td>{{ticket._source.status}}</td>
                </tr>
            </tbody>
        </table>
    `
})
export class TicketsTable {
    @Input() tickets:any = [];
}
