import { Component, Input } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';

@Component({
    selector: 'discussions-table',
    directives: [
    ...ROUTER_DIRECTIVES
    ],
    template: `
        <table class="table table-striped table-condensed text-left" *ngIf="discussions">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>User</th>
                    <th>Created on</th>
                </tr>
            </thead>
            <tbody>
                <tr *ngFor="let item of discussions">
                    <td>{{item._source.title}}</td>
                    <td>{{item._source.author}}</td>
                    <td>{{item._source.created_on}}</td>
                </tr>
            </tbody>
        </table>
    `
})
export class DiscussionsTable {
    @Input() discussions:any = [];
}
