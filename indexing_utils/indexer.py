import json
import requests
import pandas as pd
from requests.auth import HTTPBasicAuth
from sqlalchemy import create_engine


auth = HTTPBasicAuth('5c4c1248ab99a86079b6c8ff40c877b4',
                     'c771c8d4c32086e3aff68c20e4c64da8')

engine = create_engine('mysql://pharmauser:4Welcome@compute.uat.dragonglass.me/customer_care_data')


def clean_company_data(row):
    if row['data_impacts'] == '[]' or row['data_impacts'] == '':
        row['data_impacts'] = None
    else:
        row['data_impacts'] = eval(row['data_impacts'])
    return row


def people_clean_email(row):
    global docs
    global discussions
    old_email = row['email']
    first_letter = row['first_name'][0].lower()
    last_name = row['last_name'].lower()
    if "=======" in row['company_name_id']:
        row['company_name_id'].remove("=======")
    company = row['company_name_id'][0].replace('-', '')
    new_email = '{0}{1}@{2}.com'.format(first_letter, last_name, company)
    row['email'] = new_email
    docs['author'] = docs['author'].replace(to_replace=old_email, value=new_email)
    discussions['author'] = discussions['author'].replace(to_replace=old_email, value=new_email)
    return row


companies = pd.read_sql_table('company', engine, parse_dates=['last_updated'])
companies = companies.fillna('')
companies = companies.apply(clean_company_data, axis=1)

r = requests.post('http://localhost/_search/default/companies',
                  data=companies.to_json(orient='records', date_format='iso'),
                  auth=auth)

people = pd.read_sql_table('user', engine)
r = requests.post('http://localhost/_search/default/people',
                  data=companies.to_json(orient='records'), auth=auth)

tickets = pd.read_sql_table('ticket', engine)
r = requests.post('http://localhost/_search/default/tickets',
                  data=tickets.to_json(orient='records'), auth=auth)
