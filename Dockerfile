# Provides an environment to run the Customer Care app in a docker container
#
# To build and run with Docker:
#
#  $ docker build -t customercare-app .
#  $ docker run -it --rm -v $PWD/src:/customercare_app/src \
#        --name appserver -p 3000:3000 -p 3001:3001 \
#        customercare-app bash -c "cd /customercare_app && npm start:docker"
#

FROM ubuntu:14.04
RUN apt-get update -y
RUN apt-get install -y curl > /dev/null 2>&1
RUN curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
RUN apt-get install -y nodejs > /dev/null 2>&1
RUN mkdir -p /customercare_app /home/nodejs && \
    groupadd -r nodejs && \
    useradd -r -g nodejs -d /home/nodejs -s /sbin/nologin nodejs && \
    chown -R nodejs:nodejs /home/nodejs

WORKDIR /customercare_app
COPY package.json typings.json tslint.json tsconfig.json karma.conf.js ./config /customercare_app/
RUN npm install --unsafe-perm=true && npm run typings install
